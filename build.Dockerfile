ARG IMAGE
ARG GHC_VERSION
ARG PREFIX=/usr/local

FROM ${IMAGE}:${GHC_VERSION} as builder

ARG CABAL_VERSION_MIN
ARG STACK_VERSION_BUILD
ARG PREFIX
ARG MODE=install

COPY patches/*.patch /tmp/
COPY scripts/*.sh /usr/bin/

RUN mkdir -p "/tmp$PREFIX/bin" \
 && start.sh

FROM scratch

ARG IMAGE_LICENSE="MIT"
ARG IMAGE_SOURCE="https://gitlab.b-data.ch/commercialhaskell/ssi"
ARG IMAGE_VENDOR="Olivier Benz"
ARG IMAGE_AUTHORS="Olivier Benz <olivier.benz@b-data.ch>"

LABEL org.opencontainers.image.licenses="$IMAGE_LICENSE" \
      org.opencontainers.image.source="$IMAGE_SOURCE" \
      org.opencontainers.image.vendor="$IMAGE_VENDOR" \
      org.opencontainers.image.authors="$IMAGE_AUTHORS"

ARG PREFIX

COPY --from=builder "/tmp$PREFIX/bin/stack" "$PREFIX/bin/stack"
